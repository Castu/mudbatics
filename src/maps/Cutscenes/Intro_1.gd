extends Node2D

func _ready():
	$AnimationPlayer.play("Intro")

func _input(event):
	for action in InputMap.get_actions():
		if event.is_action(action):
			# warning-ignore:return_value_discarded
			get_tree().change_scene("res://menus/StartingMenu/StartingMenu.tscn")

func _on_AnimationPlayer_animation_finished(_anim_name):
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://menus/StartingMenu/StartingMenu.tscn")
