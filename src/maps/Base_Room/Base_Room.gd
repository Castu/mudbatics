extends Node2D

class_name Room

export(PackedScene) var character_scene : PackedScene
export(PackedScene) var puppet_scene : PackedScene

export(Array, PackedScene) var mud_objects : Array = []
export(Array, int) var mud_uses : Array = []

const TILE_SIZE : int = 8

var objects_spawned : Array = []

var mudman_count : int = 0
var mudmans_bag: Array = []

var character : PlayableCharacter


var game_started : bool = false
signal map_finished()

func _ready():
	assert(mud_objects.size() == mud_uses.size(), "COMO ASSIM ESSAS ARRAYS TÃO DIFERENTES")
	assert(character_scene != null, "Base_Room empty character scene!")
	assert(puppet_scene != null, "Base_Room empty puppet scene!")
	for use in mud_uses: mudman_count += use
	$HUD_Layer/HUD.setup_uses(mud_uses)
	# warning-ignore:return_value_discarded
	$HUD_Layer/HUD.connect("spawn_object", self, "spawn_object")
	spawn_character()
	game_started = true

func create_puppet(follow : Node2D) -> Node2D:
	var new_puppet = puppet_scene.instance()
	new_puppet.position = follow.position
	new_puppet.actual_follow = follow
	return new_puppet

func spawn_character(unconditional_spawn_kid: bool = false):
	if mudman_count > 0 and not unconditional_spawn_kid:
		character = character_scene.instance()

	else:
		character = character_scene.instance()
		character.kid = true
		# warning-ignore:return_value_discarded
		character.connect("die", self, "kid_died")

	character.position = $Spawn_Point.position
	$Players.call_deferred("add_child", character)

	#Spawn Puppets
	if not character.kid:
		var last_char : Node2D = character

		for _i in range(mudman_count-1):
			last_char = create_puppet(last_char)
			$Players.call_deferred("add_child", last_char)

	yield(get_tree(), "idle_frame")

	#Make Rocks soft so kid doesn't get stuck
	if character.kid:
		for obj in $Spawned_Objects.get_children():
			if obj is Rock: obj.become_soft()

func spawn_object(object_id : int) -> void:
	if not character.kid and not character.frozen:

		if mud_uses[object_id] > 0:
			mudman_count -= 1

			var object : Node2D = mud_objects[object_id].instance()
			var obj_pos : Vector2 = Vector2.ZERO
			obj_pos.x = stepify(character.position.x, TILE_SIZE)
			obj_pos.y = stepify(character.position.y, TILE_SIZE)
			object.position = obj_pos
			$Spawned_Objects.call_deferred("add_child", object)
			$Players.get_child($Players.get_child_count()-1).call_deferred("free")

#			var i : int = 1
#			var mudman = $Players.get_child($Players.get_child_count()-i)
#			while(mudman is PlayableCharacter):
#				i += 1
#				mudman = $Players.get_child($Players.get_child_count()-i)
#			mudman.call_deferred("free")

			yield(get_tree(), "idle_frame")
			$SFX/SpawnObject.play()

			mud_uses[object_id] -= 1
			objects_spawned.append([object, object_id])

			if mudman_count == 0: spawn_character()
			$HUD_Layer/HUD.update_uses(mud_uses)

		else:
			$HUD_Layer/HUD.show_error(object_id)
			$SFX/CantSpawn.play()

func kid_died():
	character.speed = Vector2.ZERO
	character.position = $Spawn_Point.position

func _input(event):
	if not game_started: return

	if event.is_action_pressed("undo"):

		if character.kid and not mudmans_bag.empty():
			character.call_deferred("free")
			character = mudmans_bag.pop_front()
			$Players.call_deferred("add_child", character)
			while !mudmans_bag.empty():
				var mudman = mudmans_bag.pop_front()
				$Players.call_deferred("add_child", mudman)

		elif objects_spawned.size() > 0:
			var action = objects_spawned.pop_back()
			if action[0] is Bomb: action[0].unplode()
			action[0].call_deferred("free")

			var was_kid : bool = false

			if character.kid: 
				character.call_deferred("free")
				was_kid = true

			yield(get_tree(), "idle_frame")

			mudman_count += 1
			mud_uses[action[1]] += 1
			$HUD_Layer/HUD.update_uses(mud_uses)

			if was_kid: spawn_character()
			else: $Players.call_deferred("add_child", create_puppet($Players.get_child($Players.get_child_count()-1)))

	elif event.is_action_pressed("interact"):
		for body in $Exit.get_overlapping_bodies():
			if body is PlayableCharacter and body.kid:
				emit_signal("map_finished")

	elif event.is_action_pressed("reset"):
		character.speed = Vector2.ZERO
		character.position = $Spawn_Point.position

	elif event.is_action_pressed("toggle_kid"):
		if mudman_count == 0: return

		elif character.kid:
			character.queue_free()
			character = mudmans_bag.pop_front()
			$Players.call_deferred("add_child", character)
			while !mudmans_bag.empty():
				var mudman = mudmans_bag.pop_front()
				$Players.call_deferred("add_child", mudman)

		else:
			for child in $Players.get_children():
				mudmans_bag.append(child)
				$Players.call_deferred("remove_child", child)
			spawn_character(true)

		yield(get_tree(), "idle_frame")

