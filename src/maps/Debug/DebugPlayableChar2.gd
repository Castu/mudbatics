extends Node2D

export(PackedScene) var mudman

func _on_PlayableCharacter_transformed():
	var mudman_instance = mudman.instance()
	mudman_instance.position = Vector2(200, -200)
	mudman_instance.connect("transformed", self, "_on_PlayableCharacter_transformed")
	add_child(mudman_instance)
