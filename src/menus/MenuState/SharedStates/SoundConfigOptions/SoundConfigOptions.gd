extends MenuState

onready var bgm_slider = $VBoxContainer/Music/HSlider
onready var sfx_slider = $VBoxContainer/SFX/HSlider
onready var bgm_index: int = AudioServer.get_bus_index("BGM")
onready var sfx_index: int = AudioServer.get_bus_index("SFX")

func _ready():
	$VBoxContainer/Music/Label.text = "Music Voolume"
	$VBoxContainer/SFX/Label.text = "SFX Volume"
	bgm_slider.value = db2linear(AudioServer.get_bus_volume_db(bgm_index))
	sfx_slider.value = db2linear(AudioServer.get_bus_volume_db(sfx_index))
	bgm_slider.grab_focus()
	


func _on_Back_pressed():
	parent_menu.go_back()


func _on_HSlider_value_changed(value):
	AudioServer.set_bus_volume_db(bgm_index, linear2db(value))


func _on_HSlider2_value_changed(value):
	AudioServer.set_bus_volume_db(sfx_index, linear2db(value))

