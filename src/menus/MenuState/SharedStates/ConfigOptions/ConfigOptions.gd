extends MenuState

func _ready():
	$VBoxContainer/Sound.grab_focus()

func _on_Sound_pressed():
	parent_menu.change_state(BaseMenu.states.SOUND_CONFIG)


func _on_Controls_pressed():
	parent_menu.change_state(BaseMenu.states.CONTROLS_CONFIG)


func _on_Back_pressed():
	parent_menu.go_back()
