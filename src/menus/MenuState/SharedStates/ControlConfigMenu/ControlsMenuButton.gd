extends HBoxContainer
class_name ControlConfigButton

signal change_key_request(keybind)

var action : String setget change_action

func change_action(new_action):
	assert(InputMap.has_action(new_action))
	action = new_action
	update_button()

func update_button() -> void:
	assert(InputMap.has_action(action))
	$Action.text = self.action.capitalize()

	for event in InputMap.get_action_list(action):
		assert(	event is InputEventKey or\
				event is InputEventJoypadButton or\
				event is InputEventJoypadMotion)

		if event is InputEventKey:
			$ButtonName.text = OS.get_scancode_string(event.scancode)

#		elif event is InputEventJoypadButton:
#			$Control_Button/Sprite.texture = JoyButtonSpriter.get_sprite(event.button_index)
#
#		elif event is InputEventJoypadMotion:
#			$Control_Button/Sprite.texture = \
#				JoyButtonSpriter.get_sprite(JoyButtonSpriter.get_motion_id(event))

func focus():
	$Button.grab_focus()

func _on_Button_focus_entered():
	$Action.modulate = Color("ff0000")
	$ButtonName.modulate = Color("ff0000")

func _on_Button_focus_exited():
	$Action.modulate = Color("ffffff")
	$ButtonName.modulate = Color("ffffff")


func _on_Button_pressed():
	self.emit_signal("change_key_request", self)
