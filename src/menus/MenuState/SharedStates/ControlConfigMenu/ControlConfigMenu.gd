extends MenuState

export(PackedScene) var config_key_button_model
export(PackedScene) var button_model

var changing_key: bool = false
var new_event_type
var new_event : InputEvent = null
var current_keybind = null

func _ready():
	for action in InputMap.get_actions():
		if not action.begins_with("ui_"):
			var button:ControlConfigButton = config_key_button_model.instance()
			button.action = action
# warning-ignore:return_value_discarded
			button.connect("change_key_request", self, "on_change_key_request")
			$VBoxContainer/Buttons.add_child(button)
			if button.action == "pause":
				button.get_node("Button").disabled = true
				button.modulate = Color("2effffff")
	var back_button: Button = button_model.instance()
	back_button.text = "Back"
# warning-ignore:return_value_discarded
	back_button.connect("pressed", self, "on_back_button_pressed")
	$VBoxContainer/Buttons.add_child(back_button)
	yield(get_tree(), "idle_frame")
	$VBoxContainer/Buttons.get_children()[0].focus()
	parent_menu.defocus()

func on_change_key_request(keybind):
	current_keybind = keybind
	changing_key = true
	new_event = null
	defocus()
	$ChangeKeyDialog.show()
	$ChangeKeyDialog/Window/Titulo.text = "Choose a button to " + keybind.action.capitalize() + " action"
	$VBoxContainer/Buttons.set_process(false)

func _input(event):
	if changing_key and event.is_pressed():
		if event is InputEventKey and not event.is_echo() and OS.get_scancode_string(event.scancode) != "Escape":
#			$ChangeKeyDialog/Window/Input/Keyboard.show()
#			$ChangeKeyDialog/Window/Input/Keyboard.text = OS.get_scancode_string(event.scancode)
			new_event_type = InputEventKey
			new_event = event
			if new_event != null:
				var action = current_keybind.action
				for event in InputMap.get_action_list(action):
					if event is new_event_type:
						InputMap.action_erase_event(action, event)
						InputMap.action_add_event(action, new_event)
				current_keybind.update_button()
				$VBoxContainer/Buttons.set_process(true)
				changing_key = false
				focus()
				$ChangeKeyDialog.hide()
#		elif event is InputEventJoypadButton:
#			$Change_Key/Windows/Window/Input/Controller.show()
#			$Change_Key/Windows/Window/Input/Keyboard.hide()
#			$Change_Key/Windows/Window/Input/Controller.texture = JoyButtonSpriter.get_sprite(event.button_index)
#			new_event_type = InputEventJoypadButton
#			new_event = event
#
#		elif event is InputEventJoypadMotion and event.axis_value != 0:
#			$Change_Key/Windows/Window/Input/Controller.show()
#			$Change_Key/Windows/Window/Input/Keyboard.hide()
#			$Change_Key/Windows/Window/Input/Controller.texture = \
#				JoyButtonSpriter.get_sprite(JoyButtonSpriter.get_motion_id(event))
#			new_event_type = InputEventJoypadButton
#			new_event = event

func defocus():
	$VBoxContainer.modulate = Color("2effffff")

func focus():
	$VBoxContainer.modulate = Color("ffffffff")

func on_back_button_pressed():
	parent_menu.focus()
	parent_menu.go_back()
