extends MenuState

func _ready():
	get_children()[0].grab_focus()

func _on_Resume_pressed():
	get_tree().paused = false
	parent_menu.hide()


func _on_MainMenu_pressed():
	get_tree().paused = false
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://menus/StartingMenu/StartingMenu.tscn")


func _on_Quit_pressed():
	get_tree().paused = false
	get_tree().quit()


func _on_Config_pressed():
	parent_menu.change_state(PauseMenu.states.CONFIGURATIONS)
