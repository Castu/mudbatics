extends BaseMenu
class_name PauseMenu

#export(Array, PackedScene) var state_scenes
#
#var current_state: int setget change_state

func _ready():
	self.show()
	pause()
	yield(get_tree(), "idle_frame")
	self.hide()

#func change_state(new_state: int) -> void:
#	current_state = new_state
#
#	for node in $MarginContainer/CenterContainer/VBoxContainer/States.get_children():
#		node.queue_free()
#
#	var state_instance: BasePauseState = state_list[current_state].instance()
#	state_instance.pause_menu = self
#	$MarginContainer/CenterContainer/VBoxContainer/States.add_child(state_instance)

func pause() -> void:
	focus()
	change_state(states.INITIAL_MENU)

func defocus() -> void:
	$MarginContainer/CenterContainer/VBoxContainer/Label.modulate = Color("2effffff")

func focus() -> void:
	$MarginContainer/CenterContainer/VBoxContainer/Label.modulate = Color("ffffffff")
