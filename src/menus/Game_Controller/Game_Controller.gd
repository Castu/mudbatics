extends Node2D

export(Array, PackedScene) var maps : Array = []
var chapter: int

var current_map : Room

func _ready():
	$ChapterBegin.play()
	assert(MapsHolder.map_list.size() > 0 , "Map array is empty!")
	maps = MapsHolder.map_list
	chapter = MapsHolder.chapter
	spawn_map()

func spawn_map():
	current_map = maps.pop_front().instance()
	$Game/Viewport.add_child(current_map)

	# warning-ignore:return_value_discarded
	current_map.connect("map_finished", self, "on_map_finished")

	$Tween.interpolate_property($Game, "rect_scale", Vector2.ZERO, Vector2.ONE,
								0.6, Tween.TRANS_SINE)
	$Tween.start()

	yield($Tween, "tween_all_completed")

func on_map_finished():
	$LevelComplete.play()

	# warning-ignore:return_value_discarded
	current_map.disconnect("map_finished", self, "on_map_finished")

	$Tween.interpolate_property($Game, "rect_scale", Vector2.ONE, Vector2.ZERO,
								0.6, Tween.TRANS_SINE)
	$Tween.start()
	yield($Tween, "tween_all_completed")

	current_map.call_deferred("free")
	yield(get_tree(), "idle_frame")

	if maps.size() > 0: spawn_map()
	else: 
		if self.chapter + 2 >= ProgressHolder.progress and self.chapter + 2 <= ProgressHolder.max_progress:
			ProgressHolder.progress = self.chapter + 2
			ProgressHolder.save_game()
		get_tree().change_scene("res://menus/StartingMenu/StartingMenu.tscn")

func _input(event):
	if event.is_action_pressed("pause"):
		get_tree().paused = not get_tree().paused
		$PauseLayer/Pause.visible = get_tree().paused
		$PauseLayer/Pause.pause()
