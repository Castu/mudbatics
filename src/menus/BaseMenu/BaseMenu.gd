extends Control
class_name BaseMenu

enum states {INITIAL_MENU, CONFIGURATIONS,SOUND_CONFIG ,CONTROLS_CONFIG, CHAPTER_SELECTION, STAGE_SELECTION, QUIT}

export(Array, PackedScene) var states_scenes
export(NodePath) var options_path

var current_state: int setget change_state
var last_states: Array

func _ready():
	change_state(0)

func change_state(state: int):
	last_states.push_front(current_state)
	current_state = state
	set_options()

func go_back():
	var next_state = last_states.pop_front()
	change_state(next_state)
	last_states.pop_front() #Tirando o estado que o change_state colocou na pilha

func set_options():
	for node in get_node(options_path).get_children():
		node.queue_free()

	var options_instance = states_scenes[current_state].instance()
	options_instance.parent_menu = self
	get_node(options_path).add_child(options_instance)

func defocus() -> void:
	pass

func focus() -> void:
	pass
