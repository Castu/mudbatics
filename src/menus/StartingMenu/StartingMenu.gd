extends BaseMenu
class_name StartingMenu

func defocus() -> void:
	$CenterContainer/VBoxContainer/Label.modulate = Color("2effffff")

func focus() -> void:
	$CenterContainer/VBoxContainer/Label.modulate = Color("ffffffff")
