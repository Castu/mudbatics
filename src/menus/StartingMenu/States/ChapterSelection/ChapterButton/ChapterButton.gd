extends Button

var chapter : int
var maps_path : Array = []
var scene_path: String

func _on_ChapterButton_pressed():
	# warning-ignore:return_value_discarded
	MapsHolder.map_list = []
	for map in maps_path: MapsHolder.map_list.append(load(map))
	MapsHolder.chapter = chapter
	get_tree().change_scene(scene_path)
