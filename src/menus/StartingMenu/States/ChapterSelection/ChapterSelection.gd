extends BaseMenuState

export(Array, String, DIR) var chapters_path : Array = []
export(PackedScene) var chapter_button_model
export(PackedScene) var button_model

func _ready():
	var chapters_directory: = Directory.new()

	for i in range(ProgressHolder.progress):#chapter_folder in chapters_path:

		var error = chapters_directory.open(chapters_path[i])
		assert(error == 0, "Error opening chapter folder!")

		# warning-ignore:return_value_discarded
		chapters_directory.list_dir_begin()

		var maps_array : Array = []
		var file

		while file != "":
			file = chapters_directory.get_next()
			if file != "." and file != ".." and file != "":
				maps_array.append(chapters_path[i] + "/" + file)
		
		var button = chapter_button_model.instance()
		button.maps_path = maps_array
		button.chapter = i
		button.scene_path = "res://menus/Game_Controller/Game_Controller.tscn"
		button.text = chapters_path[i].right(chapters_path[i].find_last("/")+1).replace("_"," ")
		self.add_child(button)

	var back_button: Button = button_model.instance()
	back_button.text = "Back"
	self.add_child(back_button)
	# warning-ignore:return_value_discarded
	back_button.connect("pressed", self, "on_back_pressed")

	self.get_children()[0].grab_focus()

func on_back_pressed() -> void:
	change_state(StartingMenu.states.INITIAL_MENU)
