extends VBoxContainer
class_name BaseMenuState

var parent_menu: Control

func change_state(next_state) -> void:
	parent_menu.current_state = next_state
