extends BaseMenuState

func _ready():
	self.get_children()[0].grab_focus()

func _on_Quit_pressed():
	get_tree().quit()

func _on_Select_Chapter_pressed():
	change_state(StartingMenu.states.CHAPTER_SELECTION)


func _on_Configurations_pressed():
	change_state(StartingMenu.states.CONFIGURATIONS)
