extends Label

export(bool) var swap_text_on_ready : bool = true

func _ready():
	if swap_text_on_ready : swap_text()

func swap_text() -> void:
	#Get individual words
	var words : Array = self.text.split(" ", false)
	var index : int = 0

	#Find and replace words that start with #
	for word in words:
		if word[0] == "#":
			word = word.lstrip("#")
			assert(InputMap.has_action(word), "Action_Label invalid action \""+word+"\"")

			for input_event in InputMap.get_action_list(word):
				if input_event is InputEventKey:
					words[index] = OS.get_scancode_string(input_event.scancode)
					break

		index += 1

	#Set new text
	self.text = words.pop_front()
	for word in words: self.text += " " + word
