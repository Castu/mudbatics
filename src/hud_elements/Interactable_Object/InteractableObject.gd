extends Node2D
export(Array, String) var actions
var enabled: bool = false setget set_enabled

func set_enabled(value : bool) -> void:
	enabled = value

	if enabled:
		$Label/AnimationPlayer.play("Interact")
		$Label.show()

	else: 
		$Label/AnimationPlayer.stop()
		$Label.hide()

func _ready():
	assert(not actions.empty(), "InteractableObject empty actions")
	$Label.text += "#"+actions[0]

	if actions.size() > 1:
		var index : int = 1
		while index < actions.size():
			$Label.text += " or #"+actions[index]
			index += 1

	$Label.swap_text()

func _process(_delta):
	$Label.rect_position.x = -($Label.rect_size.x * $Label.rect_scale.x)/2

