extends Control

signal spawn_object(object_id)

onready var powers := $Powers/Power_Organizer

var selected_power : int = 0 setget set_selected
var max_powers : int = -1

func set_selected(new_selected : int) -> void:
	if max_powers == 0: return

	selected_power = new_selected

	if selected_power < 0: selected_power = max_powers-1
	elif selected_power >= max_powers: selected_power = 0

	$Powers/Power_Selector.rect_position = \
	$Powers/Power_Organizer.get_child(selected_power).rect_position + Vector2(49,-5)

func setup_uses(mud_uses : Array):
	var i : int = 0
	for power in powers.get_children():
		if mud_uses[i] <= 0: power.call_deferred("free")
		else: power.get_child(0).text = str(mud_uses[i])
		i += 1

	yield(get_tree(), "idle_frame")

	max_powers = $Powers/Power_Organizer.get_child_count()

	if max_powers > 1:
		$Powers/Power_Selector.show()
		self.selected_power += 0

func update_uses(mud_uses : Array):
	for power in powers.get_children():
		power.get_child(0).text = str(mud_uses[power.object_id])
		if mud_uses[power.object_id] > 0: power.modulate.a = 1
		else: power.modulate.a = 0.5

	if mud_uses[powers.get_child(selected_power).object_id] == 0:
		for _i in range(powers.get_child_count()):
			self.selected_power += 1
			if mud_uses[powers.get_child(selected_power).object_id] > 0:
				break

func show_error(power : int):
	powers.get_child(power).shake()

func _input(event):
	if max_powers == 0: return

	if event.is_action_pressed("object_select"):
		self.emit_signal("spawn_object", powers.get_child(selected_power).object_id)

	elif event.is_action_pressed("left_object_select"):
		self.selected_power -= 1

	elif event.is_action_pressed("right_object_select"):
		self.selected_power += 1
