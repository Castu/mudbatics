extends TextureRect

export(int) var object_id : int = -1

var original_pos : Vector2 = Vector2.ZERO

func _ready():
	assert(object_id >= 0, "Object_Id on Power_Counter not set!")

func shake() -> void:
	original_pos = self.rect_position
	$Shake_Proc.start()
	$Shake_Dur.start()
	self.modulate.b = 0.2
	self.modulate.g = 0.2
	self.rect_position = original_pos + Vector2(rand_range(-5, 5), rand_range(-5, 5))

func _on_Shake_Proc_timeout():
	self.rect_position = original_pos + Vector2(rand_range(-5, 5), rand_range(-5, 5))

func _on_Shake_Dur_timeout():
	$Shake_Proc.stop()
	self.modulate.b = 1
	self.modulate.g = 1
	self.rect_position = original_pos
