extends Node

const max_progress := 3

onready var save_file_name: String = "mudbatics.save"

var progress : int = 1

"""
Version : 1.0 (string)
progress : int
"""

func load_game() -> void:
	var file = File.new()
	var save_info : Dictionary
	
	if !file.file_exists("user://" + save_file_name):
		save_game()
	
	file.open("user://" + save_file_name, File.READ)
	
	save_info = parse_json(file.get_as_text())
	progress = save_info["progress"]

func save_game() -> void:
	var file = File.new()
	var file_info :Dictionary = {
		"Version" : "1.0",
		"progress" : progress
	}
	
	file.open("user://" + save_file_name, File.WRITE)
	
	file.store_string(to_json(file_info))
	file.close()
