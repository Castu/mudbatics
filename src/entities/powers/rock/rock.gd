extends StaticBody2D

class_name Rock

var solid : bool = false

func _ready():
	become_soft()

func become_soft():
	self.modulate.a = 0.6
	solid = false
	$collision.call_deferred("set", "disabled", true)

func explode():
	self.hide()
	$collision.call_deferred("set", "disabled", true)

func unplode():
	self.show()
	$collision.call_deferred("set", "disabled", false)

func _physics_process(_delta):
	if not solid and $empty_checker.get_overlapping_bodies().size() == 0:
		solid = true
		self.modulate.a = 1
		$collision.call_deferred("set", "disabled", false)
