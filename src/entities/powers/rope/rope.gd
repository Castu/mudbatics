extends Area2D
class_name Rope

var rope_size : float = 0.0

func _ready():
	$Floor_Finder.force_raycast_update()

	rope_size = 120
	var size = Vector2(0, rope_size)

	if $Floor_Finder.is_colliding():
		size = $Floor_Finder.get_collision_point() - (self.global_position - Vector2(0, 16)) #Sprite and Collider offset
		$Floor_Finder.cast_to = size
		rope_size = size.y

	$Shape.position.y = (size.y/2) - 16
	$Shape.scale.y = size.y/120

	$Sprite.points[1].y = 0
	$Floor_Finder.enabled = false


func _process(delta):
	if $Sprite.points[1].y < rope_size:
		$Sprite.points[1].y = min($Sprite.points[1].y + rope_size*2*delta, rope_size)
