extends Area2D


func _on_body_entered(body):
	if body is PlayableCharacter:
		$SFX.play()
		body.speed.y = -700
		body.current_state = body.states.FALLING
		$Animation.stop()
		$Animation.play("Spring")
