extends Area2D

export(PackedScene) var bullet_res : PackedScene

func shoot(player: PlayableCharacter):
	$Interact_Area.call_deferred("set", "disabled", true)

	player.frozen = true
	player.hide()
	player.speed = Vector2.ZERO

	var bullet : KinematicBody2D = bullet_res.instance()
	bullet.position = Vector2(0, 0)
	bullet.z_index = -1
	bullet.speed.x *= sign(self.scale.x)
	bullet.modulate = player.modulate

	$Animation.play("Shoot")
	$SFX.play()
	$Tube/Smoke.restart()

	player.speed = Vector2(bullet.speed.x*(-0.5), 0)
	self.call_deferred("add_child", bullet)
	yield(bullet,"hit")

	player.global_position = bullet.global_position
	player.frozen = false
	player.show()

	bullet.call_deferred("free")
	$Interact_Area.call_deferred("set", "disabled", false)

func _on_LeftDetector_body_entered(_body):
	self.position.x += 5

func _on_RightDetector_body_entered(_body):
	self.position.x -= 15
