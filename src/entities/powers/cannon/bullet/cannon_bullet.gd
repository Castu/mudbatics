extends KinematicBody2D

signal hit()

var speed : Vector2 = Vector2(500, 0)
var spin_dir : float = 10.0

func _ready():
	if speed.x < 0: spin_dir *= -1

func _physics_process(delta):
	# warning-ignore:return_value_discarded
	move_and_slide(speed, Vector2.UP)
	self.rotation += spin_dir*PI*delta

	if get_slide_count() > 0:
		self.emit_signal("hit")
