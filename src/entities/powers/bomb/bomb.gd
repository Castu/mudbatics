extends Area2D
class_name Bomb

var explode_arr: Array = []

func explode():

	$SFX.play()
	$Sprite.hide()
	$Explode_Parts.restart()
	$Timer.start()

	for body in self.get_overlapping_bodies():
		if body is Rock:
			explode_arr.append(body)
			body.explode()

func _on_Timer_timeout():
	self.hide()

func unplode():
	for body in explode_arr:
		body.unplode()
