tool
extends Node2D

export(PackedScene) var arrow : PackedScene

enum dir {LEFT, RIGHT, UP, DOWN}
export(dir) var shoot_dir : int = dir.RIGHT setget change_dir
export(float) var shoot_timer : float = 0.7
export(float) var shoot_speed : float = 300.0

var arrow_dir : Vector2 = Vector2.RIGHT

func change_dir(new_dir : int) -> void:
	shoot_dir = new_dir
	match(new_dir):

		dir.UP:
			$Sprite.rotation_degrees = -135
			arrow_dir = Vector2.UP

		dir.DOWN:
			$Sprite.rotation_degrees = 45
			arrow_dir = Vector2.DOWN

		dir.RIGHT:
			$Sprite.rotation_degrees = -45
			arrow_dir = Vector2.RIGHT

		dir.LEFT:
			$Sprite.rotation_degrees = 135
			arrow_dir = Vector2.LEFT

func _ready():
	assert(arrow != null)
	_on_Shoot_timeout()
	$Shoot_Timer.wait_time = shoot_timer
	$Shoot_Timer.start()

func _on_Shoot_timeout():
	if not Engine.editor_hint:
		var new_arrow : Arrow = arrow.instance()
		new_arrow.position = self.position
		new_arrow.dir = arrow_dir
		new_arrow.speed = shoot_speed
		get_parent().call_deferred("add_child", new_arrow)
		$Animation.stop()
		$Animation.play("Shoot")
