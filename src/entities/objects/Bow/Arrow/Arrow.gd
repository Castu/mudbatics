extends KinematicBody2D

class_name Arrow

var speed : float = 0.0
var dir : Vector2 = Vector2.ZERO

func _ready():
	assert(speed > 0.0, "Arrow speed is ZERO")

	match(dir):
		Vector2.RIGHT:
			continue

		Vector2.LEFT:
			$Sprite.rotation_degrees = -135

		Vector2.UP:
			$Sprite.rotation_degrees = -45

		Vector2.DOWN:
			$Sprite.rotation_degrees = 135

func _physics_process(delta):
	var collision : KinematicCollision2D = self.move_and_collide(speed*dir*delta)

	if collision != null:
		if collision.collider is PlayableCharacter and collision.collider.kid:
			collision.collider.die()

		self.speed = 0.0
		$Animation.play("break")
		$Collision.call_deferred("set", "disabled", true)
		yield($Animation, "animation_finished")

		self.call_deferred("free")
