extends Area2D

func _on_Spike_body_entered(body):
	if body is PlayableCharacter:
		if body.kid:
			body.die()
