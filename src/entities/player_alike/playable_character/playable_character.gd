extends KinematicBody2D
class_name PlayableCharacter

signal die()

export(bool) var kid = false

const ACCEL: float = 700.0
const FRICTION: float = 0.7
const GRAV : float = 1500.0
const CLIMBING_VELOCITY : float = 250.0

var max_speed = 250.0
var jump_force = 500.0
var frozen = false

enum states {IDLE, WALKING, JUMPING, FALLING, WALLGRAB, CLIMBING}
export(states) var initial_state

onready var current_state : int = initial_state setget change_state

var speed: Vector2 = Vector2(0, 100)

func get_rotation() -> float:
	return $Sprite.rotation

func change_state(new_state : int) -> void:
	frozen = false
	self.set_collision_mask_bit(2, true)
	current_state = new_state
	#play anim new_state

func _ready():
	$SFX/Jump.stream.set_loop(false)

	if kid: 
		self.modulate = Color(1,1,1)
		self.max_speed /= 2
		self.jump_force /= 1.5
		$Sprite.frame = 300

func _physics_process(delta):
	if not frozen:
		var grav_modifier: float = 1.0
		var dir := Vector2.ZERO
		dir.x = Input.get_action_strength("right") - Input.get_action_strength("left")
		dir.y = Input.get_action_strength("down") - Input.get_action_strength("up")

		if dir.x != sign(speed.x): speed.x *= FRICTION

		speed.x += ACCEL*delta*dir.x

		if sign(speed.x) != 0: $Sprite.scale.x = sign(speed.x)
		if abs(speed.x) >= max_speed: speed.x = sign(speed.x)*max_speed

		if (Input.is_action_pressed("up") or Input.is_action_pressed("down")) and current_state != states.JUMPING:
			for body in $BodyArea.get_overlapping_areas():
				if body is Rope:
					self.position.x = body.position.x
					self.speed.x = 0
					self.change_state(states.CLIMBING)

		match(current_state):
			states.IDLE:
				$CoyoteJumpBuffer.start()
				resolve_bunnyhop_in_buffer()
				if not self.is_on_floor(): self.change_state(states.FALLING)
				elif abs(speed.x) > 0.30: self.change_state(states.WALKING)

			states.WALKING:
				$CoyoteJumpBuffer.start()
				resolve_bunnyhop_in_buffer()
				if not self.is_on_floor():
					self.change_state(states.FALLING)

				elif abs(speed.x) < 0.30:
					self.change_state(states.IDLE)
					speed = Vector2.ZERO
			
			states.JUMPING:
				$CoyoteJumpBuffer.stop()
				$BunnyHopBuffer.stop()
				if speed.y >= 0: current_state = states.FALLING
				else: $Sprite.rotation += PI*2*delta*sign(speed.x)
			
			states.FALLING:
				if self.is_on_floor():
					$Sprite.rotation = 0
					self.change_state(states.IDLE)

				elif ((is_wall_in_leftarea2d() and Input.is_action_pressed("left")) or 
					(is_wall_in_rightarea2d() and Input.is_action_pressed("right"))):
					$Sprite.rotation = 0
					self.change_state(states.WALLGRAB)

				else:
					$Sprite.rotation += PI*2*delta*sign(speed.x)
					grav_modifier = 1.5

			states.WALLGRAB:
				$CoyoteJumpBuffer.start()
				if ((is_wall_in_leftarea2d() and Input.is_action_pressed("left")) or 
					(is_wall_in_rightarea2d() and Input.is_action_pressed("right"))):
					grav_modifier = 0.0
					speed = Vector2.ZERO

				else: self.change_state(states.FALLING)

			states.CLIMBING:
				$Sprite.rotation = 0
				if is_in_rope_range():
					self.set_collision_mask_bit(2, false)
					$CoyoteJumpBuffer.start()
					grav_modifier = 0.0
					speed.y = CLIMBING_VELOCITY * dir.y
				else:
					self.change_state(states.FALLING)

		speed.y += grav_modifier * GRAV * delta

		speed = move_and_slide(speed, Vector2.UP)
		is_interactable_message()

func _input(event):
	if not frozen:
		if event.is_action_pressed("jump") and !event.is_echo():
			if not $CoyoteJumpBuffer.is_stopped(): jump()
			else: $BunnyHopBuffer.start()

		elif current_state == states.JUMPING and event.is_action_released("jump"):
			self.speed.y /= 4
			current_state = states.FALLING

		elif event.is_action_pressed("interact"):
			for area in $BodyArea.get_overlapping_areas():
				if area.is_in_group("cannon"):
					area.shoot(self)
					return

func die() -> void:
	$SFX/Death.play()
	emit_signal("die")

func is_in_rope_range() -> bool:
	for body in $BodyArea.get_overlapping_areas():
		if body is Rope:
			return true
	return false

func resolve_bunnyhop_in_buffer():
	if !$BunnyHopBuffer.is_stopped():
		jump()

func is_wall_in_leftarea2d() -> bool:
	if kid: return false
	elif $LeftWallSentinel.get_overlapping_bodies().size() > 0: return true
	else: return false

func is_wall_in_rightarea2d() -> bool:
	if kid: return false
	elif $RightWallSentinel.get_overlapping_bodies().size() > 0: return true
	else: return false

func jump() -> void:
	$SFX/Jump.play()
	self.speed.y = -jump_force
	self.current_state = states.JUMPING

#Acho que da para só fazer um grupo "interagivel" ao invés de olhar um a um
func is_interactable_message():
	for area in $BodyArea.get_overlapping_areas():
		if area.is_in_group("rope") or area.is_in_group("cannon") or (area.is_in_group("exit") and kid):
			area.get_node("InteractableObject").enabled = true

func _on_BodyArea_area_exited(area):
	if area.is_in_group("rope") or area.is_in_group("cannon") or area.is_in_group("exit"):
		area.get_node("InteractableObject").enabled = false
