extends Sprite

export(NodePath) var node_to_follow : NodePath
export(int, 0, 100, 1) var follow_delay : int = 5

var actual_follow : Node2D
var path : Array = []

func _ready():
	assert(node_to_follow != null and actual_follow != null, "MudPuppet node to follow is null!")
	if actual_follow == null: actual_follow = self.get_node(node_to_follow)

func _physics_process(_delta):
	path.append([actual_follow.position, actual_follow.get_rotation()])
	if path.size() > follow_delay:
		var next_pos = path.pop_front()
		self.position = next_pos[0]
		self.rotation = next_pos[1]
